<?php
class Building{
	protected $name;
	private $floors;
	private $address;
 
	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

    public function getName(){
		return $this->name;
	}

    public function getFloors(){
		return $this->floors;
	}

    public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		if(strLen($name)!==0){
			$this->name = $name;
		}
	}
}

class Condominium extends Building{
    public function getName(){
		return $this->name;
	}
    public function setName($name){
		if(strLen($name)!==0){
			$this->name = $name;
		}
	}
}


?>