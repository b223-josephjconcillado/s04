<?php require_once('./code.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04: Access Modifiers</title>
</head>
<body>
    <h1>Building</h1>
    <?php $building = new Building("Caswynn Building",8,"Timog Avenue, Quezon City, Philippines"); ?>
    <p>The name of the building is <?= $building->getName(); ?>.</p>
    <p>The <?= $building->getName()?> has <?= $building->getFloors(); ?> floors.</p>
    <p>The <?= $building->getName()?> is located at <?= $building->getAddress(); ?>.</p>
    <?php $building->setName("Caswynn Complex");?>
    <p>The name of the building has been changed to <?= $building->getName(); ?>.</p>


    <h1>Condominium</h1>
    <?php $condominium = new Condominium("Enzo Condo",5,"Buendia Avenue, Makati City, Philippines"); ?>
    <p>The name of the building is <?= $condominium->getName(); ?>.</p>
    <p>The <?= $condominium->getName()?> has <?= $condominium->getFloors(); ?> floors.</p>
    <p>The <?= $condominium->getName()?> is located at <?= $condominium->getAddress(); ?>.</p>
    <?php $condominium->setName("Enzo Tower");?>
    <p>The name of the building has been changed to <?= $condominium->getName(); ?>.</p>


</body>
</html>